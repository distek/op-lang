---
date: 2016-03-09T20:08:11+01:00
title: Embedding
weight: 30
---

## Background

We've built our VT with embeddability in mind.  This means that you can seamlessly add VT functionality to an existing application.  To achieve this, there are a few system level requirements:

- Must have a .NET Core compatible host to run the server on
- Must have a HTTP connection between the host and your application
- Must have a Javascript and Websocket capable browser in your application

For example, we can run our server on a RaspberryPi and have the UI running on an iPad, using a `WKWebView` embedded into an iOS application.

## Example

This will run the server application on port `5000`.  To keep this example simple, we'll run the VT server on a Windows PC.  The server is written for .NET Core which means it can run on Windows, Linux, and macOS.

### Step 1: Set up the Server

First, download the Windows `http` server version from the downloads page.  Extract it somewhere you'll remember, like `C:\tools\VT`.  At this point you'll want to choose the type of CAN adapter you want.  Open up `networkconfig.json` and you'll see that VT-Anywhere is configured to use a Peak USB adapter by default.  If you'd like to use a different adapter, or make your own adapter work, check the `CAN Adapter` page.

Once you have a CAN adapter configured, you can just run `Distek.Vt.Web.exe`.  You should see something like the following:
```
Hosting environment: Production
Content root path: C:\tools\VT
Now listening on: http://localhost:5000
Application started. Press Ctrl+C to shut down.
```

On your PC, you can open a web browser an navigate to `localhost:5000` to see the VT running.

### Step 2: Add the VT to your Application

Now that we've got the VT running, all that's left to do is add a web browser control to our application and point it to the correct web host.  In this example, we'll use `WKWebView`, built into iOS to display the VT.  Since there are already plenty of good tutorials on setting up `WKWebView` I won't write another one.  [Here](https://www.hackingwithswift.com/read/4/2/creating-a-simple-browser-with-wkwebview) is an example.  

If you follow that tutorial, make sure to disable navigation:
```
webView.allowsBackForwardNavigationGestures = false
```

TODO: create sample projects for different platforms and put them on gitlab 
