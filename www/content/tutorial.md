---
date: 2016-03-08T21:07:13+01:00
title: op-lang tutorial
weight: 0
---

## Getting Started

To get started, you'll need to download the `op-lang` compiler.  Check the [releases page on GitLab](https://gitlab.com/distek/op-lang/tags) for the latest version.  Download it somewhere on your PC, and add it to your command line path.  To make sure you're ready to go, open a command line and enter
```
Distek.OpLang.exe
```
You should see a usage message:
```
Compiling object pool...

        h:header        Write a .h file
        i:input         Input file
        j:json          Write JSON output for VT server
        o:output        Output file
        w:watch         Watch for input file changes and auto update
```

## Your First Object Pool

Let's start by writing the minimum possible object pool definition.  The minimum loadable object pool consists of:

* Working Set object
* Child in the working set object
* Any required children of that object
* Data mask

To keep things simple we'll just put an `OutputString` on our `WorkingSet`.  Here's how we create an empty `WorkingSet` object:

```
WorkingSet : myWs {
   BackgroundColour: RGB(0, 153, 0),
   ActiveMaskId: defaultMask,
   Selectable: True,
   VersionLabel: '      1',
   Children [
      Reference(os, 5, 27)
   ]
}
```

Notice that we have a few parameters here, `defaultMask` and `os` that are references that need to be defined.  For most parameters, such as `ActiveMaskId` you can either set a static value, or use a value reference.  Here's how we'll define `defaultMask`:
```
DataMask: defaultMask {
   BackgroundColour: Colour(4),
}
```
This is just an empty mask, but it also illustrates another way colours may be defined.  More information regarding this can be found in the [Language Spec](/language) color section.

To finish out our test pool, we'll have to provide a definition for the object `os`.  As you could probably guess, this is an `OutputString` object.  All `OutputString` objects require a `FontAttributes` object to define their appearance.  This is another instance where we'll use a reference:
```
OutputString : os {
   Size: (60, 51),
   Location: (0, 27),
   Transparent: True,
   BackgroundColour: RGB(255, 255, 255),
   Value: 'hello!',
   Wrap: False,
   FontAttributes: workingSetFont,
   Justification: HorizontalCenter,
}

FontAttributes : workingSetFont
{
   FontColour: RGB(0, 0, 0),
   FontSize: 4,
   FontType: 0,
   FontStyle: 0,
}
```

Here's the complete example: [mvop.op](https://gitlab.com/distek/op-lang/blob/master/sample/mvop.op).


## Compiling

Once you've got an object pool defined, it's time to convert it to a binary .iop file and load it up on your VT.  The minimum command line arguments are input and output file names which we'll use like so:
```
C:\workspace> Distek.OpLang -i mvop.op -o test
Compiling object pool...
Found 0 file imports
Creating myWs
Creating os
Creating workingSetFont
Creating defaultMask
Populating myWs from myWs
reference: defaultMask
Populating os from os
reference: workingSetFont
reference: HorizontalCenter
Populating workingSetFont from workingSetFont
Populating defaultMask from defaultMask
Object pool compilation complete
```

Note that this output has verbose logging enabled so we can see what's going on.  Once this completes with no errors, you should have a file in the current directory, `test.iop`.  Load this file into your [favorite VT](https://distek-products.gitlab.io/vt-server/), using your [favorite VT client library](https://distek-products.gitlab.io/virtec/) and see how it looks.

Here's the working set showing up in the main menu:

![workingset](/ws.png)

If you select our demo working set icon, you'll see a lovely color index 4 page:

![defaultMask](/defaultMask.png)

Beautiful.


