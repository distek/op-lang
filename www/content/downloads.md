---
date: 2016-03-09T20:08:11+01:00
title: Downloads
weight: 30
---

## Current Releases

To embed the VT into an existing application download one of the `http` versions below

### v0.0.14

* Added pattern fill for rectangles
* Corrected object ID sent when a button or key is used by an object pointer

### Download

* [Windows http (x64)](https://s3.amazonaws.com/distek-products/vt-server/vt-win-x64.zip)
* [Windows http (x32)](https://s3.amazonaws.com/distek-products/vt-server/vt-win-x32.zip)
* [Windows Desktop (x64)](https://s3.amazonaws.com/distek-products/vt-server/vt-server Setup 0.0.14.exe)
* [Windows Desktop (x32)](https://s3.amazonaws.com/distek-products/vt-server/vt-win-desktop-x32.zip)


### 0.0.13

* Added time and date request handler
* Improved font size handling
* Preserving whitespace in output string
* Changed string spacing to get closer to correct font width
* Fixed string justification bug when setting v4 option bits
* Improved handling of change audio command
* Added modal for changing value of input number
* Implemented latched buttons
* Fixed input list with no current value selected
* Fixed response to change/hide object command
* Corrected TP/ETP behavior when in one session and CF asks to start another one
* Changed behavior of web handler to not ignore messages when navigating back to the main page
