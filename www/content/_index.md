---
date: 2016-03-08T21:07:13+01:00
title: op-lang
type: index
weight: 0
---

## Introduction

Have you ever had to develop an ISOBUS object pool with other people?  If you have, you might have noticed that it can be really inconvenient!  Current tools are based on XML documents and project files that are really hard to diff.  That's where `op-lang` comes to the rescue!

`op-lang` is a MIT-licensed open source project started by [DISTek Integration](https://distek.com) to help us develop ISOBUS VT client applications faster with more flexibility.

## Features

- Diff-able object pools
- Increased re-use of components through packaging (documentation coming soon)
- Compile time styles and themes
- Integration with our VT server
- Easy localization
- .IOP file export

### Feature Roadmap

We haven't had time to implement all of the features we want to see yet!  Here's an idea of where we're going:

- .IOP file import
- Constraint based layout 
- Data binding when using VIRTEC
- VS integration + syntax highlighting 

