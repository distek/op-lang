---
date: 2016-03-09T20:08:11+01:00
title: CAN Adapter
weight: 30
---

VT Anywhere ships with a few different CAN adapters.  You can configure which adapter to use at startup.  You can also supply a custom CAN adapter.

## Configuration

Currently the following CAN adapters are available:

- Peak USB
- UDP Network
- SocketCAN

By default, VT Anywhere is configured to use the Peak USB adapter.  If you'd like to use a different adapter, overwrite `networkconfig.json` in the execution directory with `network.[type].json` where `[type]` is the type of adapter you want to use.


## UDP Adapter

The UDP adapter is intended to provide interprocess communication between a client and server on the same PC.  To use this adpater, configure VT Anywhere to use it as outlined above and set your client up to communicate over UDP.

The protocol is very simple, so updating a client to use the UDP adapter shouldn't be difficult.  The basics here are:

- Use UDP Multicast
- Use multicast address `239.0.0.222`
- Use port `25000`

The port and address are configurable in `networkconfig.json` if the defaults don't work for you.  The message format is:

- Header
- DLC
- 8 byte data array

Here's some sample code for constructing a message to send over UDP:
```
char msg[32] = { 0x00 };
msg[0] = (uint8_t)((packet->Header.Identifier & 0xFF000000) >> 24);
msg[1] = (uint8_t)((packet->Header.Identifier & 0x00FF0000) >> 16);
msg[2] = (uint8_t)((packet->Header.Identifier & 0x0000FF00) >> 8);
msg[3] = (uint8_t)((packet->Header.Identifier & 0x000000FF));
msg[4] = (uint8_t)packet->DLC;
for (i = 0; i < packet->DLC; i++) {
   msg[5 + i] = packet->Data[i];
}
```

To read incoming messages, just listen to the multicast socket and do the opposite.

## Custom Adapter

To implement a custom `INetwork`, start out with the following steps:

1. Create a new .NET Core library project.  The name of this project must contain the word "Network"
2. Add a project reference to `Distek.Network`
3. Add a new class, `MyNetwork` to your library project
4. Implement `INetwork` in your class.  It should look like this:

```
using System;
using Distek.Network;

namespace MyCompany.TestNetwork
{
    public class SampleNetwork : INetwork
    {
      public string Key { get { return "sample"; } }

      public void Connect()
      {
      // TODO: Add code to set up a connection to your CAN adapter if necessary
      }

      public void Send(CanMessage message)
      {
      // TODO: Add code to send a message using your CAN adapter
      }

      public void Receive(Action<CanMessage> handler)
      {
      // TODO: Add code to receive data from your CAN adapter
      }
    }
}
```

See the `INetwork` class documentation for details about each interface method.  The least obvious method to implement is `Receive`.
This method will return immediately, so you'll want to spawn a thread inside of it that runs for the lifetime of the application.  
You can make your `Receive` method async and have it `await` an infinite task.  Implementations commonly take the following form:

```
public async void Receive(Action<CanMessage> handler)
{
   // Set up the task
   await Task.Run(async () =>
   {
      // Run forever
      while (true)
      {
        // Call a method here that does a read from your CAN network.
        // If you don't have an async read method, you can await Task.Delay(1),
        // but it's recommended to have an async read method
        var recv = await myCanNetwork.ReceiveAsync();
        // Convert your native message format to a CanMessage
        var parsed = ParseMyMessageType(recv.Buffer);
        // Execute the callback that was passed in to this method
        handler(parsed);
      }
   });
}
```

Since we're using a dependency injection container, you can also register anything else you want to use in your 
custom network class here and it'll be injected through the constructor.  An example of this could be
network configuration parameters.

## Testing Your Custom Network

`Distek.NetworkTest` is set up to automatically load `INetwork` implementations from the current directory. 
To test your custom network, add a project reference to your library project from `Distek.CanLog`.  Building
the project should copy your library to the output directory and the CAN log program should find it and 
load it.  If you get an exception, make sure your custom network .dll file name contains the word "Network."

`Distek.NetworkTest` will print out any messages it has received, and it will send a counter value every second.
When you run the program you should see output similar to the following:

```
Listening for CAN messages.  Press a key to exit
0x18FF0101      8       00 00 00 00 00 00 00 00
0x18FF0101      8       01 00 00 00 00 00 00 00
0x18FF0101      8       02 00 00 00 00 00 00 00
0x18FF0101      8       03 00 00 00 00 00 00 00
```
The message we're seeing logged here is the test counter that we're sending.
