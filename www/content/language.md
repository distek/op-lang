---
date: 2016-03-08T21:07:13+01:00
title: op-lang language
weight: 0
---

## Status

The `op-lang` language and implementation are still under development.  Changes to the language may occur at any time.  However, the language is functional today and can be used to create non-trivial object pools.

Here's an overview of feature priority

  - ~~Initial language specification~~
  - ~~Read single file object pool project~~
  - ~~Output .iop file~~
  - ~~Multiple file project~~
  - ~~Components~~ Working, but not fully tested
  - ~~Styles~~ Can be implemented via property references
  - ~~Localization~~ Compile time localization can be implemented via property references
  - Compile time layout constraints
  - Run-time layout constraints
  - Import existing IOP file
  - Data binding
  - VS Integration

## Language Specification

### General format for ISOBUS object definition:
```
[value] denotes a mandatory value
{value} denotes an optional value
    
[ObjectType] : {ObjectName} {
   {param-id} : [param-value],
   {param-id} : [param-value],
   {param-id} : [param-value],
}
```

For an object to be referenced by an external object, it must include a value for `ObjectName`. Objects declared inside of a Children block do not need to have a name to be used by the parent object.

Root element of the project will be a working set.

```
WorkingSet : [name] {
   BackgroundColor: RGB(0, 153, 0),
   Width: 160,
   Height: 80,
   Selectable: true
   ActiveMask: defaultMask,
   VersionLabel: "      1",
   Children: [
      OutputString {
         FontAttributes: workingSetFont,
         Size: (160, 51),
         Location: (0, 27),
         Transparent: True,
         BackgroundColor: RGB(255, 255, 255),
         Value: "My App",
         Wrap: false,
         HorizontalAlignment: Horizontal.Center
      }
   ]
}
```

## Keywords
- `import` imports a file to the current project
  - `import com.distek.Defaults.op` would import a set of default components from `com.distek.Defaults.op`
- `component` defines a component
  - `component TextButton {` exports a component named `TextButton`
- `property` defines a component property
- `constant` for non-changing value
  - `constant Color.Black: rgb(0, 0, 0)` would make `Color.Black` usable from other components
  - This could be used for `enum` types in the standard.  For example, `Justification` of `OutputString` has 8 values that it can be assigned
Binding Syntax

To create a data binding to a programmatic variable, use the following syntax:

        Value: <model_name.variable_name>

This will create a variable in a .h file named `variable_name` in a struct named `model_name`. C code will also be generated to automatically bind that value to the displayed value.


## Color Specification
- Named
- `FromIndex`
- `rgb(r, g, b)`

## Layout

In the example above we’re setting the size and location of a string manually, which we’d like to avoid.  Instead we can build in constraint based layout to figure this out for us.  Instead of the size and location we could use something like:

    Layout: Constraints.FillAndCenter

For something more complicated, maybe a group of rectangles that act like as an indicator we could use:
```
LayoutStack {
  Orientation: Horizontal
  Children [
    Rectangle {
        Width: 50%
        BackgroundColor: <indicator.left>
    },
    Rectangle {
        Width: 50%
        BackgroundColor: <indicator.right>
    },
  ]
}
```

Implementation Note: This may end up being a VIRTEC-only thing.  The idea driving constraint layout would be to do a better job of scaling object pools for different sized VTs.  Since the scaling has to be done at run-time, there’s probably not a great way of exporting the layout data for a general VT client library.
Implementation Note: Layouts should be implemented as components so that it’s easy to add them.

## Componentization (Pacakging)

It’d be nice to be able to reuse components across different projects.  Examples of this could be something like the grid of square indicators built for Dawn being reused in the Loup planter VT project.  The component would take parameters such as number of squares to display, and lay itself out in the mask.

To be able to reuse components we’ll need to package them into a single file.  That file will also probably need:

- Metadata about the component
- Customization parameters.  For example, a graph component may need a number variable to be passed in to it

We’ll also need to be able to dynamically write object IDs for subcomponents so they don’t conflict with other ids.  This will only be a concern for generating general .iop files.  If we’re using VIRTEC with data binding, we won’t need to know about object IDs at all.

This will be really useful for stuff like a button with text.  Instead of:
```
Button {
  Children [
    OutputString {
      Text: 'hello'
    }
  ]
}
```
You could have a composite control that knows the button should have text in it:
```
TextButton {
  Text: 'hello'
}
```

The component itself would be defined something like:

```
component TextButton {
  // this gets mapped onto the OutputString's text property
  property Text: buttonText.Text
  property Width: button.Width
  property Height: button.Height
      
  Button : button {
     // these are default values - property values override them
     Width: 100
     Height: 30
     Children [
        OutputString : buttonText {
           Text: ""
        }
     ]
  }
}
```

This custom button type would probably have parameters for justification, font size, etc.  
