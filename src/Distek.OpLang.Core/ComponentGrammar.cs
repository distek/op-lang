﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprache;

namespace Distek.OpLang.Core
{
   public class Component
   {
      public string Name { get; set; }
      public IEnumerable<IsoValue<OpLangLiteral>> Properties { get; set; }
      public IEnumerable<IsoObject> Objects { get; set; }
   }

   public class ComponentGrammar
   {
      public static Parser<Component> Component =
         from keyword in Parse.String("Component").Token()
         from separator in Parse.Char(':').Token()
         from name in PoolParser.Identifier
         from open in Parse.Char('{').Token()
         from values in PoolParser.Value.Many()
         from o in Parse.Char('[').Token()
         from objects in PoolParser.PoolObject.Many()
         from c in Parse.Char(']').Token()
         from close in Parse.Char('}').Token()
         select new Component() { Name = name, Properties = values, Objects = objects };
   }
}
