﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprache;
using System.Drawing;
using System.Diagnostics;

namespace Distek.OpLang.Core
{
   public class PoolParser
   {
      static CommentParser Comment = new CommentParser("//", "/*", "*/", "\r\n");

      public static Parser<string> Identifier = Parse.LetterOrDigit.AtLeastOnce().Text().Token();

      public static Parser<char> CharValue = Parse.AnyChar.Except(Parse.Char('\''));

      public static Parser<OpLangLiteral> StringValue = from open in Parse.Char('\'')
                             from content in CharValue.Many().Text()
                             from close in Parse.Char('\'')
                             select new OpLangLiteral(content, IsoType.String);

      public static Parser<OpLangLiteral> BooleanValue = from str in Parse.String("True").Text().Or(Parse.String("False").Text()).Token()
                              select new OpLangLiteral(str, IsoType.Boolean);

      public static Parser<OpLangLiteral> IntegerValue = from minus in Parse.String("-").Text().Optional()
                              from digits in Parse.Decimal.Text().Token()
                              select new OpLangLiteral(((minus.IsDefined ? minus.Get() : "") + digits), IsoType.Number);

      public static Parser<OpLangLiteral> PointValue = (from open in Parse.Char('(')
                                                     from x in IntegerValue
                                                     from separator in Parse.Char(',')
                                                     from y in IntegerValue
                                                     from close in Parse.Char(')')
                                                     select new OpLangLiteral(new Point((int)x.Get(), (int)y.Get())));

      public static Parser<OpLangLiteral> IndexedColor = (from rgb in Parse.String("Colour").Text().Token()
                                                          from open in Parse.Char('(')
                                                          from idx in IntegerValue
                                                          from close in Parse.Char(')')
                                                          select idx);

      // removed RGB colour since it doesn't make a lot of sense since you can't use all of the colors
      // could bring it back, or maybe replace it with something better
      //public static Parser<OpLangLiteral> Colour = RgbColor.XOr(IndexedColor);
      public static Parser<OpLangLiteral> Colour = IndexedColor;

      public static Parser<OpLangLiteral> Reference = 
         from val in Parse.AnyChar.Except(Parse.Char(',')).Many().Text()
         select new OpLangLiteral(val, IsoType.Reference);

      // Note: it's important to declare these parsers in the order they're used.  If this is 
      // declared before anything on the right side it'll throw a null reference exception
      public static Parser<OpLangLiteral> Literal =
                              StringValue
                              .XOr(IntegerValue)
                              .XOr(PointValue)
                              .XOr(BooleanValue)
                              .XOr(Colour)
                              // we have precedence issues with references and color/boolean.  as a workaround i changed color/boolean
                              // to be case sensitive and use upper case.  that means that references need to start with lower case characters
                              // alternatively we could wrap references in [] or something similar
                              // i don't know why the parser isn't reading the whole string for keywords, we might need a tokenizer
                              // could try https://github.com/datalust/superpower
                              .XOr(Reference);

      public static Parser<IsoValue<OpLangLiteral>> Value = (from id in Identifier
                      from separator in Parse.Char(':').Token()
                      from value in Literal
                      from end in Parse.Char(',').Token()
                      select new IsoValue<OpLangLiteral>() { Identifier = id, Value = value });

      public static Parser<IsoObject> PoolObject = (from id in Identifier
                        from colon in Parse.Char(':').Token()
                        from name in Identifier
                        from open in Parse.Char('{').Token()
                        from line in Value.Many()
                        from children in ChildArray.Optional()
                        from close in Parse.Char('}').Token()
                        select new IsoObject() { Identifier = id, Name = name, Value = line, Children = children.GetOrDefault() });

      public static Parser<IsoValue<OpLangLiteral>> Constant =
         from keyword in Parse.String("const").Token()
         from val in Value
         select val;

      // TODO: need to remove invalid characters
      public static Parser<string> Filename =
         from name in Parse.CharExcept('\n').Many().Text().Token()
         select name;

      public static Parser<FileReference> Import =
         from keyword in Parse.String("import").Token()
         from val in Filename
         select new FileReference(val, _path);

      public static Parser<ParsedPool> Pool = 
         //from comments in Comment.SingleLineComment.AtLeastOnce()
         from constants in Constant.Many()
         from imports in Import.Many()
         from components in ComponentGrammar.Component.Many()
         from objects in PoolObject.Many()
         select new ParsedPool() { FileReferences = imports, Constants = constants, Objects = objects.ToList(), Components = components };

      public static Parser<IEnumerable<ChildReference>> ChildArray =
         from keyword in Parse.IgnoreCase("Children").Token()
         from start in Parse.Char('[').Token()
         from obj in ChildReference.Many()
         from end in Parse.Char(']').Token()
         select obj;

      public static Parser<ChildReference> ChildReference =
         from keyword in Parse.IgnoreCase("Reference").Token()
         from start in Parse.Char('(').Token()
         from name in Identifier
         from separator in Parse.Char(',')
         from x in IntegerValue
         from separator2 in Parse.Char(',')
         from y in IntegerValue
         from close in Parse.Char(')').Token()
         select new ChildReference() { Name = name, Location = new Point((int)x.Get(), (int)y.Get()) };

      private static string _path;
      
      public ParsedPool GeneratePool(string pool, string path)
      {
         _path = path;

         var parsedObjects = Pool.Parse(pool);

         Console.WriteLine("Found {0} file imports", parsedObjects.FileReferences.Count());

         foreach (var file in parsedObjects.FileReferences)
         {
            var importPool = PoolParser.Pool.Parse(file.Content());
            Console.WriteLine("Imported file has {0} objects", importPool.Objects.Count);
            // TODO: consider recursing into imported files looking for more imports, or just 
            // keep things top-down
            parsedObjects.Objects.AddRange(importPool.Objects);
            parsedObjects.Constants = parsedObjects.Constants.Concat(importPool.Constants);
            parsedObjects.Components = parsedObjects.Components.Concat(importPool.Components);
         }

         return parsedObjects;
      }
   }
}
