﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Core
{
   public class ChildReference
   {
      public string Name { get; set; }
      public Point Location { get; set; }
   }

   public class IsoObject
   {
      // TODO: we should use the code from VT server for objects since it already exists
      // unfortunately it has a bunch of extra stuff that should be removed before we 
      // bring it into this project
      public string Identifier { get; set; }

      public string Name { get; set; }

      public IEnumerable<IsoValue<OpLangLiteral>> Value { get; set; }

      public IEnumerable<ChildReference> Children { get; set; }

      public IsoObject()
      {
         Children = new List<ChildReference>();
      }
   }

   public class ParsedPool
   {
      public IEnumerable<FileReference> FileReferences { get; set; }
      public IEnumerable<IsoValue<OpLangLiteral>> Constants { get; set; }
      public List<IsoObject> Objects { get; set; }
      public IEnumerable<Component> Components { get; set; }
   }
}

