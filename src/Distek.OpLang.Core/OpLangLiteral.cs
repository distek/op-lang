﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Distek.OpLang.Core
{
   public class OpLangLiteral
   {
      public string Value { get; set; }
      public Point PointValue { get; set; }
      public int ColorValue { get; set; }

      public IsoType ValueType { get; set; }

      public OpLangLiteral(IsoType type)
      {
         ValueType = type;
      }

      public OpLangLiteral(Point point)
      {
         this.ValueType = IsoType.Point;
         this.PointValue = point;
      }

      public OpLangLiteral(int color)
      {
         this.ValueType = IsoType.Color;
         this.ColorValue = color;
      }

      public OpLangLiteral(string value, IsoType type)
      {
         Value = value;
         ValueType = type;
      }

      public object Get()
      {
         switch (ValueType)
         {
            case IsoType.Boolean:
               return (Value.ToLower() == "true") ? true : false;

            case IsoType.Number:
               return Convert.ToInt32(Value);

            case IsoType.String:
               return Value;

            case IsoType.Color:
               return ColorValue;

            case IsoType.Point:
               return PointValue;

            default:
               return null;
         }
      }


   }
}
