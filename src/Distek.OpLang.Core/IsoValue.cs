﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Core
{
   /// <summary>
   /// Represents a name/value pair of an runtime defined type
   /// Note that this might not be a reasonable way to represent data coming out of the parser -- 
   /// may need concrete types for each type of value we support
   /// </summary>
   /// <typeparam name="T"></typeparam>
   public class IsoValue<T>
   {
      public string Identifier { get; set; }
      public T Value { get; set; }
   }
}
