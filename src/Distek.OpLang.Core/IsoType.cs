﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Core
{
   public enum IsoType
   {
      String,
      Number,
      Boolean,
      Color,
      Point,
      Reference,
      ChildReference,
   }
}
