﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprache;
using System.IO;

namespace Distek.OpLang.Core
{
   public class FileReference
   {
      public string Filename { get; set; }
      public string Fullname { get; set; }

      public FileReference(string filename, string path)
      {
         Filename = filename;
         Fullname = Path.Combine(path, filename);
      }

      public string Content()
      {
         return File.ReadAllText(Fullname);
      }
   }
}
