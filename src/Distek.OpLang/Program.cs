﻿using Distek.OpLang.Core;
using Distek.OpLang.Generator;
using Fclp;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang
{
   class Program
   {
      public class Args
      {
         public string InputFile { get; set; }
         public string OutputFile { get; set; }
         public bool WriteJson { get; set; }
         public bool LiveUpdate { get; set; }
         public bool WriteHeader { get; set; }
      }

      static void Main(string[] args)
      {
         Console.WriteLine("Compiling object pool...");
         var a = new FluentCommandLineParser<Args>();
         a.Setup(arg => arg.InputFile).As('i', "input").Required().WithDescription("Input file");
         a.Setup(arg => arg.OutputFile).As('o', "output").Required().WithDescription("Output file");
         a.Setup(arg => arg.WriteJson).As('j', "json").WithDescription("Write JSON output for VT server");
         a.Setup(arg => arg.LiveUpdate).As('w', "watch").WithDescription("Watch for input file changes and auto update");
         a.Setup(arg => arg.WriteHeader).As('h', "header").WithDescription("Write a .h file");
         a.SetupHelp("?", "help").Callback(text => Console.WriteLine(text));
         var options = a.Parse(args);
         if(options.HasErrors)
         {
            a.HelpOption.ShowHelp(a.Options);
            return;
         }

         var dir = new FileInfo(a.Object.InputFile).DirectoryName;

         var p = new PoolParser();
         var constants = GetConstants();
         var input = constants +  File.ReadAllText(a.Object.InputFile);
         var pool = p.GeneratePool(input, dir);
         var outFilename = a.Object.OutputFile + ".iop";
         var generator = new IopGenerator();
         var o = generator.Generate(pool);
         using (var os = new BinaryWriter(File.OpenWrite(outFilename)))
         {
            foreach (var x in o)
               x.Write(os);
         }

         if (a.Object.WriteJson)
         {
            var outfile = a.Object.OutputFile + ".json";
            File.WriteAllText(outfile, JsonConvert.SerializeObject(o, new JsonSerializerSettings
            {
               ContractResolver = new CamelCasePropertyNamesContractResolver()
            }));
         }
         if (a.Object.WriteHeader)
         {
            GenerateHeader(o, a.Object.OutputFile);
         }

         if (a.Object.LiveUpdate)
         {
            var watcher = new FileSystemWatcher(dir, "*.op");
            watcher.Changed += (s, e) =>
            {
               Console.WriteLine("File changed, recompiling");
               input = constants + File.ReadAllText(a.Object.InputFile);
               p = new PoolParser();
               var watchedPool = p.GeneratePool(input, dir);
               o = generator.Generate(watchedPool);
               var outfile = a.Object.OutputFile + ".json";
               File.WriteAllText(outfile, JsonConvert.SerializeObject(o, new JsonSerializerSettings
               {
                  ContractResolver = new CamelCasePropertyNamesContractResolver()
               }));
            };
            watcher.EnableRaisingEvents = true;
            Console.WriteLine("Watching for changes in {0}", dir);
            Console.WriteLine("Press enter to exit");
            Console.Read();
         }

         Console.WriteLine("Object pool compilation complete");
      }

      static void GenerateHeader(IEnumerable<IsoVtObject> objects, string filename)
      {
         var defines = new List<string>()
         {
            "#define ISO_DESIGNATOR_WIDTH             160",
            "#define ISO_DESIGNATOR_HEIGHT            80",
            "#define ISO_MASK_SIZE                    480",
            "#define ISO_VERSION_LABEL             \"       \"",
            "#define MASK_WIDTH                    480",
            "#define MASK_HEIGHT                    480",
         };
         foreach(var o in objects)
         {
            // this is the virtec format but we want designer right now
            //defines.Add(string.Format("#define {0}\t\t\t((ObjectID_T) {1})", o.Name, o.ObjectId));
            defines.Add(string.Format("#define {0}\t\t\t{1}", o.Name, o.ObjectId));
         }
         var f = filename + ".iop.h";
         File.WriteAllLines(f, defines.ToArray());
      }

      static string GetConstants()
      {
         var asm = Assembly.GetExecutingAssembly();
         var streamReader = new StreamReader(asm.GetManifestResourceStream("Distek.OpLang.isoconstants.op"));
         return streamReader.ReadToEnd();
      }
   }
}
