﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   [ObjectType(IsobusObjectType.InputString)]
   public class InputString : IsoVtObject
   {
      public string Type { get { return "inputString"; } }

      public Point Size { get; set; }
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public int BackgroundColour { get; set; }
      public int FontAttributes { get; set; }
      public int InputAttributes { get; set; }
      public int Options { get; set; }
      public int VariableReference { get; set; }
      public int Justification { get; set; }
      public int Length { get; set; }
      public string Value { get; set; }
      public bool Enabled { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public InputString(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 1;
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.BackgroundColour = reader.ReadByte();
         this.FontAttributes = reader.ReadUInt16();
         this.InputAttributes = reader.ReadUInt16();
         this.Options = reader.ReadByte();
         this.VariableReference = reader.ReadUInt16();
         this.Justification = reader.ReadByte();
         this.Length = reader.ReadByte();
         this.Value = new string(reader.ReadChars(this.Length));
         this.Enabled = reader.ReadBoolean();
         var macroCount = reader.ReadByte();

         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.InputString);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((UInt16)this.FontAttributes);
         writer.Write((UInt16)this.InputAttributes);
         writer.Write((byte)this.Options);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((byte)this.Justification);
         writer.Write((byte)this.Length);
         // NOTE: the encoding might not be right here - probably has to be utf8
         writer.Write(Encoding.UTF8.GetBytes(this.Value));
         writer.Write(this.Enabled);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
