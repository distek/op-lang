﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.9.3 Output Number Object
   [ObjectType(IsobusObjectType.OutputNumber)]
   public class OutputNumber : IsoVtObject
   {
      public string Type { get { return "outputNumber"; } }

      public Point Size { get; set; }
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }

      public int BackgroundColour { get; set; }
      public int FontAttributes { get; set; }
      public int Options { get; set; }
      public int VariableReference { get; set; }
      public int Value { get; set; }
      public int Offset { get; set; }
      public float Scale { get; set; }
      public int NumberOfDecimals { get; set; }
      public bool Format { get; set; }
      public int Justification { get; set; }
      public bool Transparent { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputNumber(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 1;
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.BackgroundColour = reader.ReadByte();
         this.FontAttributes = reader.ReadUInt16();
         this.Options = reader.ReadByte();
         this.VariableReference = reader.ReadUInt16();
         this.Value = reader.ReadInt32();               // TODO this is supposed to be unsigned not signed
         this.Offset = reader.ReadInt32();
         this.Scale = reader.ReadSingle();
         this.NumberOfDecimals = reader.ReadByte();
         this.Format = reader.ReadBoolean();
         this.Justification = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }

         this.Transparent = (this.Options & 0x01) == 0x01;

         Console.WriteLine("Output number scale: {0}", this.Scale);
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.OutputNumber);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((UInt16)this.FontAttributes);
         writer.Write((byte)this.Options);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((UInt32)this.Value);
         writer.Write((Int32)this.Offset);
         writer.Write((float)this.Scale);
         writer.Write((byte)this.NumberOfDecimals);
         writer.Write((byte)(this.Format ? 1 : 0));
         writer.Write((byte)this.Justification);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
