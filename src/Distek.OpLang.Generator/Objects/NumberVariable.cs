﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.13.2 Number Variable Object
   [ObjectType(IsobusObjectType.NumberVariable)]
   public class NumberVariable : IsoVtObject
   {
      public string Type { get { return "numberVariable"; } }

      public int Value { get; set; }

      public NumberVariable(int objectId, string name) : base(objectId, name)
      { }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Value = reader.ReadInt32();
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.NumberVariable);
         writer.Write(this.Value);
      }
   }
}
