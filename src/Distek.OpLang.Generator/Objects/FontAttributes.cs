﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   // Annex B.14.2 Font Attributes Object
   [ObjectType(IsobusObjectType.FontAttributes)]
   public class FontAttributes : IsoVtObject
   {
      public string Type { get { return "fontAttributes"; } }

      public int FontColour { get; set; }
      public int FontSize { get; set; }
      public int FontType { get; set; }
      //public FontStyle FontStyle { get; set; }
      public int FontStyle { get; set; }
      

      public IList<ReferencedMacro> Macros { get; set; }

      public FontAttributes(int objectId, string name) : base(objectId, name)
      {
         Macros = new List<ReferencedMacro>();
         FontColour = 0;
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.FontColour = reader.ReadByte();
         this.FontSize = reader.ReadByte();
         this.FontType = reader.ReadByte();
         //this.FontStyle = (FontStyle)(reader.ReadByte());
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)23);
         writer.Write((byte)this.FontColour);
         writer.Write((byte)this.FontSize);
         writer.Write((byte)this.FontType);
         writer.Write((byte)this.FontStyle);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
