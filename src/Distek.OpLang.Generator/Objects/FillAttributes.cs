﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   // Annex B.14.4 Fill Attributes Object
   [ObjectType(IsobusObjectType.FillAttributes)]
   public class FillAttributes : IsoVtObject
   {
      public string Type { get { return "fillAttributes"; } }

      public int FillType { get; set; }
      public int FillColour { get; set; }
      public int FillPattern { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public FillAttributes(int objectId, string name) : base(objectId, name)
      {
         FillColour = 0;
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.FillType = (FillType)reader.ReadByte();
         this.FillColour = reader.ReadByte();
         this.FillPattern = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.FillAttributes);
         writer.Write((byte)this.FillType);
         writer.Write((byte)this.FillColour);
         writer.Write((UInt16)this.FillPattern);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
