﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.10.4 Output Ellipse Object
   [ObjectType(IsobusObjectType.OutputEllipse)]
   public class OutputEllipse: IsoVtObject
   {
      public string Type { get { return "outputEllipse"; } }

      public int LineAttributes { get; set; }
      public Point Size { get; set; }
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public int EllipseType { get; set; }
      public int StartAngle { get; set; }
      public int EndAngle { get; set; }
      public int FillAttributes { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputEllipse(int objectId, string name) : base(objectId, name)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.LineAttributes = reader.ReadUInt16();
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.EllipseType = reader.ReadByte();
         this.StartAngle = reader.ReadByte();
         this.EndAngle = reader.ReadByte();
         this.FillAttributes = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.OutputEllipse);
         writer.Write((UInt16)this.LineAttributes);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.EllipseType);
         writer.Write((byte)this.StartAngle);
         writer.Write((byte)this.EndAngle);
         writer.Write((UInt16)this.FillAttributes);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
