﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.9.2 Output String Object
   [ObjectType(IsobusObjectType.OutputString)]
   public class OutputString : IsoVtObject
   {
      public string Type { get { return "outputString"; } }
      
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public int BackgroundColour { get; set; }
      public int FontAttributes { get; set; }
      public int Options
      {
         get;
         set;
      }
      public int VariableReference { get; set; }
      public int Justification { get; set; }
      public int Length { get; set; }
      public string Value { get; set; }
      public bool Transparent { get; set; }
      public bool AutoWrap { get; set; }
      public bool WrapOnHyphen { get; set; }
      public Point Size { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputString(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 1;
         VariableReference = 0xFFFF;
         Macros = new List<ReferencedMacro>();
         Value = "";
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.BackgroundColour = reader.ReadByte();
         this.FontAttributes = reader.ReadUInt16();
         this.Options = reader.ReadByte();
         this.VariableReference = reader.ReadUInt16();
         this.Justification = reader.ReadByte();
         this.Length = reader.ReadUInt16();
         var bytes = reader.ReadBytes(this.Length);
         this.Value = Encoding.ASCII.GetString(bytes);
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }

         this.Transparent = (this.Options & 0x01) == 0x01;
         this.AutoWrap = (this.Options & 0x02) == 0x02;
         this.WrapOnHyphen = (this.Options & 0x04) == 0x04;
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         var options = (Convert.ToInt32(this.Transparent) | (Convert.ToInt32(this.AutoWrap) << 1) | (Convert.ToInt32(this.WrapOnHyphen) << 2));

         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)11);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((UInt16)this.FontAttributes);
         writer.Write((byte)options);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((byte)this.Justification);
         writer.Write((UInt16)this.Value.Length);
         var b = Encoding.UTF8.GetBytes(this.Value);
         writer.Write(b);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
