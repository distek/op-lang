﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   // Annex B.16 Macro Object
   [ObjectType(IsobusObjectType.Macro)]
   public class Macro : IsoVtObject
   {
      public string Type { get { return "macro"; } }

      public int Length { get; set; }
      public IList<byte> Bytes { get; set; }

      public Macro(int objectId, string name) : base(objectId, name)
      {
         Bytes = new List<byte>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Length = (int)reader.ReadUInt16();
         this.Bytes = reader.ReadBytes(this.Length).ToList();
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.Macro);
         writer.Write((UInt16)this.Length);
         writer.Write(this.Bytes.ToArray());
      }
   }
}
