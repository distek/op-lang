﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   // Annex B.14.5 Input Attributes Object
   [ObjectType(IsobusObjectType.InputAttributes)]
   public class InputAttributes : IsoVtObject
   {
      public string Type { get { return "inputAttributes"; } }

      public int ValidationType { get; set; }
      public int Length { get; set; }
      public string Value { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public InputAttributes(int objectId, string name) : base(objectId, name)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.ValidationType = reader.ReadByte();
         this.Length = reader.ReadByte();
         //this.Value = new string(reader.ReadChars(this.Length));
         var chars = reader.ReadBytes(this.Length);
         if (chars.Length > 2 && chars[0] == 0xFF && chars[1] == 0xFE)
         {
            //   var buf = new byte[this.Length];
            //   for (var i = 0; i < buf.Length; i++)
            //      buf[i] = reader.ReadByte();
            //   this.Value = Encoding.Unicode.GetString(buf, 0, this.Length);
            System.Diagnostics.Debug.WriteLine("widechar???");
         }
         else
         {
            this.Value = Encoding.ASCII.GetString(chars, 0, this.Length);
         }
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.InputAttributes);
         writer.Write((byte)this.ValidationType);
         writer.Write((byte)this.Length);
         writer.Write(this.Value.ToCharArray());
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
