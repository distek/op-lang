﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   // Annex B.14.3 LineAttributes Object
   [ObjectType(IsobusObjectType.LineAttributes)]
   public class LineAttributes : IsoVtObject
   {
      public string Type { get { return "lineAttributes"; } }
      
      public int LineColour { get; set; }
      public int LineWidth { get; set; }
      public int LineArt { get; set; }
 
      public IList<ReferencedMacro> Macros { get; set; }

      public LineAttributes(int objectId, string name) : base(objectId, name)
      {
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.LineColour = reader.ReadByte();
         this.LineWidth = reader.ReadByte();
         this.LineArt = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.LineAttributes);
         writer.Write((byte)this.LineColour);
         writer.Write((byte)this.LineWidth);
         writer.Write((UInt16)this.LineArt);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
