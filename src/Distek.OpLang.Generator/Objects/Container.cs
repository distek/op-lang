using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.4 Container Object
   [ObjectType(IsobusObjectType.Container)]
   public class Container : IsoVtObject, IHasChildren
   {
      public string Type { get { return "container"; } }

      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public Point Size { get; set; }
      public bool Hidden { get; set; }

      public IList<ReferencedObject> Children { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public Container(int objectId, string name) : base(objectId, name)
      {
         Hidden = false;
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.Hidden = reader.ReadBoolean();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.Children.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.Container);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write(this.Hidden);
         writer.Write((byte)this.Children.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.Children)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
