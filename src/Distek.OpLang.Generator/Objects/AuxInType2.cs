﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   [ObjectType(IsobusObjectType.ExternalObjectDefinition)]
   public class AuxInType2 : IsoVtObject
   {
      public AuxInType2(int objectId) : base(objectId)
      {
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         var bgColour = reader.ReadByte();
         var funcAttrs = reader.ReadByte();
         var num = reader.ReadByte();
         for (var i = 0; i < num; i++)
         {
            reader.ReadUInt16();
            reader.ReadInt16();
            reader.ReadInt16();
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
      }
   }
}
