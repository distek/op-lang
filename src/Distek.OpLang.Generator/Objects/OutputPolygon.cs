﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   public class PolygonPoint
   {
      public int Index { get; set; }
      public int XLocation { get; set; }
      public int YLocation { get; set; }
   }

   // Annex B.10.5 Output Polygon Object
   [ObjectType(IsobusObjectType.OutputPolygon)]
   public class OutputPolygon : IsoVtObject
   {
      public string Type { get { return "outputPolygon"; } }
      
      public Point Size { get; set; }
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public int LineAttributes { get; set; }
      public int FillAttributes { get; set; }
      public int PolygonType { get; set; }

      public IList<PolygonPoint> Points { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public OutputPolygon(int objectId, string name) : base(objectId, name)
      {
         Points = new List<PolygonPoint>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.LineAttributes = reader.ReadUInt16();
         this.FillAttributes = reader.ReadUInt16();
         this.PolygonType = reader.ReadByte();
         var pointsCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < pointsCount; i++)
         {
            var o = new PolygonPoint()
            {
               Index = i,
               XLocation = reader.ReadUInt16(),
               YLocation = reader.ReadUInt16(),
            };
            this.Points.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.OutputPolygon);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((UInt16)this.LineAttributes);
         writer.Write((UInt16)this.FillAttributes);
         writer.Write((byte)this.PolygonType);
         writer.Write((byte)this.Points.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var p in this.Points)
         {
            writer.Write((UInt16)p.XLocation);
            writer.Write((UInt16)p.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
