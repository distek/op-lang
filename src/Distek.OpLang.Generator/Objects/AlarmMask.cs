using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.3 Alarm Mask Object
   [ObjectType(IsobusObjectType.AlarmMask)]
   public class AlarmMask : IsoVtObject, IHasChildren
   {
      public string Type { get { return "alarmMask"; } }

      public int BackgroundColour {get;set;}
      public int SoftKeyMaskObjectId {get;set;}
      public int Priority {get; set; }
      public int AcousticSignal { get; set; }

      public IList<ReferencedObject> Children { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public AlarmMask(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 12;
         SoftKeyMaskObjectId = 0xFFFF;
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.BackgroundColour = reader.ReadByte();
         this.SoftKeyMaskObjectId = reader.ReadUInt16();
         this.Priority = reader.ReadByte();
         this.AcousticSignal = reader.ReadByte();
         var objectCount = reader.ReadByte();

         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.Children.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.AlarmMask);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((UInt16)this.SoftKeyMaskObjectId);
         writer.Write((byte)this.Priority);
         writer.Write((byte)this.AcousticSignal);
         writer.Write((byte)this.Children.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.Children)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
