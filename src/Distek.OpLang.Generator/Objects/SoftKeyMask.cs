using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Distek.OpLang.Core;

namespace Distek.OpLang.Generator
{
   // Annex B.5 Soft Key Mask Object
   [ObjectType(IsobusObjectType.SoftKeyMask)]
   public class SoftKeyMask : IsoVtObject, IHasChildren
   {
      public string Type { get { return "softkeyMask"; } }

      public int BackgroundColour { get; set; }

      public IList<ReferencedObject> Children { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public SoftKeyMask(int objectId, string name) : base(objectId, name)
      {
         // default background color
         BackgroundColour = 7;
         //ChildObjects = new List<ReferencedObject>();
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.BackgroundColour = reader.ReadByte();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               // softkeys don't have a location
               //XLocation = reader.ReadInt16(),
               //YLocation = reader.ReadInt16()
            };
            this.Children.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.SoftKeyMask);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((byte)this.Children.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.Children)
         {
            writer.Write((UInt16)c.ObjectId);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
