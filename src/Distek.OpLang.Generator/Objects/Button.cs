using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.7 Button Object
   [ObjectType(IsobusObjectType.Button)]
   public class Button : IsoVtObject, IHasChildren
   {
      public string Type { get { return "isoButton"; } }

      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public Point Size { get; set; }
      public int BackgroundColour { get; set; }
      public int BorderColour { get; set; }
      public int KeyCode { get; set; }
      public int Options { get; set; }
      public IList<ReferencedObject> Children { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public Button(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 59;
         BorderColour = 0;
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.BackgroundColour = reader.ReadByte();
         this.BorderColour = reader.ReadByte();
         this.KeyCode = reader.ReadByte();
         this.Options = reader.ReadByte();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.Children.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.Button);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((byte)this.BorderColour);
         writer.Write((byte)this.KeyCode);
         writer.Write((byte)this.Options);
         writer.Write((byte)this.Children.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.Children)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
