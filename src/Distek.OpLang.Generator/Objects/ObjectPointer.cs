﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Distek.OpLang.Generator
{
   // Annex B.15 Object Pointer Object
   [ObjectType(IsobusObjectType.ObjectPointer)]
   public class ObjectPointer : IsoVtObject
   {
      public string Type { get { return "objectPointer"; } }
      
      public int Value { get; set; }

      public ObjectPointer(int objectId, string name) : base(objectId, name)
      {
      }


      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.Value = reader.ReadUInt16();
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.ObjectPointer);
         writer.Write((UInt16)this.Value);
      }
   }
}
