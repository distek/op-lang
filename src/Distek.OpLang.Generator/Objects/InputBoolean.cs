﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   [ObjectType(IsobusObjectType.InputBoolean)]
   public class InputBoolean : IsoVtObject
   {
      public string Type { get { return "inputBoolean"; } }

      public int BackgroundColour { get; set; }
      public int ForegroundColour { get; set; }
      public int Width { get; set; }
      public int VariableReference { get; set; }
      public bool Value { get; set; }
      public bool Enabled { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public InputBoolean(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 0;
         ForegroundColour = 1;
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.BackgroundColour = reader.ReadByte();
         this.Width = reader.ReadUInt16();
         this.ForegroundColour = reader.ReadUInt16();
         this.VariableReference = reader.ReadUInt16();
         this.Value = reader.ReadBoolean();
         this.Enabled = reader.ReadBoolean();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.InputBoolean);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((UInt16)this.Width);
         // Very confusing - the spec calls this ForegroundColour, but it's a pointer to a font attributes object
         writer.Write((UInt16)this.ForegroundColour);
         writer.Write((UInt16)this.VariableReference);
         writer.Write(this.Value);
         writer.Write(this.Enabled);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
