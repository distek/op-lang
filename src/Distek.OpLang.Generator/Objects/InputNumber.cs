﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   [Flags]
   public enum InputNumberOptions
   {
      Transparent = 0x01,
      DisplayLeadingZeros = 0x02,
      DisplayZeroAsBlank = 0x04,
      Truncate = 0x08,
   }

   [ObjectType(IsobusObjectType.InputNumber)]
   public class InputNumber : IsoVtObject
   {
      public string Type { get { return "inputNumber"; } }

      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public Point Size { get; set; }
      public int BackgroundColour { get; set; }
      public int FontAttributes { get; set; }
      public InputNumberOptions Options { get; set; }
      public int Options2 { get; set; }
      public int VariableReference { get; set; }
      public int Justification { get; set; }
      public int Value { get; set; }
      public int MinValue { get; set; }
      public int MaxValue { get; set; }
      public int Offset { get; set; }
      public float Scale { get; set; }
      public int NumberOfDecimals { get; set; }
      public bool Format { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public InputNumber(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 1;
         Value = 0;
         VariableReference = 0xFFFF;
         MinValue = 0;
         MaxValue = 100;
         Scale = 1;
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         //Width = reader.ReadUInt16();
         //Height = reader.ReadUInt16();
         BackgroundColour = reader.ReadByte();
         FontAttributes = reader.ReadUInt16();
         Options = (InputNumberOptions)reader.ReadByte();
         VariableReference = reader.ReadUInt16();
         Value = reader.ReadInt32();
         MinValue = (int)reader.ReadUInt32();
         MaxValue = (int)reader.ReadUInt32();
         Offset = reader.ReadInt32();
         Scale = reader.ReadSingle();
         NumberOfDecimals = reader.ReadByte();
         Format = reader.ReadBoolean();
         Justification = reader.ReadByte();
         Options2 = reader.ReadByte();

         var macroCount = reader.ReadByte();

         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.InputNumber);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((UInt16)this.FontAttributes);
         writer.Write((byte)this.Options);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((Int32)this.Value);
         writer.Write((UInt32)this.MinValue);
         writer.Write((UInt32)this.MaxValue);
         writer.Write((Int32)this.Offset);
         writer.Write(this.Scale);
         writer.Write((byte)this.NumberOfDecimals);
         writer.Write(this.Format);
         writer.Write((byte)this.Justification);
         writer.Write((byte)this.Options2);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
