using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.1 Working Set Object
   [ObjectType(IsobusObjectType.WorkingSet)]
   public class WorkingSet : IsoVtObject, IHasChildren
   {
      public string Type { get { return "workingSet"; } }

      public int BackgroundColour { get; set; }
      public bool Selectable { get; set; }
      public int ActiveMaskId { get; set; }

      public IList<ReferencedObject> Children { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }
      public IList<string> LanguageCode { get; set; }

      public WorkingSet(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 1;
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
         LanguageCode = new List<string>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.BackgroundColour = reader.ReadByte();
         this.Selectable = reader.ReadBoolean();
         this.ActiveMaskId = reader.ReadUInt16();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         var languageCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.Children.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         for (var i = 0; i < languageCount; i++)
         {
            LanguageCode.Add(new string(reader.ReadChars(2)));
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)0);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((byte)(this.Selectable == true ? 1 : 0));
         writer.Write((UInt16)this.ActiveMaskId);
         writer.Write((byte)this.Children.Count);
         writer.Write((byte)this.Macros.Count);
         writer.Write((byte)this.LanguageCode.Count);
         foreach(var c in this.Children)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
         foreach(var l in this.LanguageCode)
         {
            var bytes = Encoding.UTF8.GetBytes(l);
            writer.Write(bytes);
         }
      }
   }
}
