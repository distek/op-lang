﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   [ObjectType(IsobusObjectType.InputList)]
   // Note: this object is a special case when it comes to validation ;(
   public class InputList : IsoVtObject, IHasChildren
   {
      public string Type { get { return "inputList"; } }

      public Point Size { get; set; }
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public int VariableReference { get; set; }
      public int Value { get; set; }
      public int NumberOfItems { get; set; }
      public int Options { get; set; }

      public IList<ReferencedObject> Children { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public InputList(int objectId, string name) : base(objectId, name)
      {
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.VariableReference = reader.ReadUInt16();
         this.Value = reader.ReadByte();
         this.NumberOfItems = reader.ReadByte();
         this.Options = reader.ReadByte();
         var macroCount = reader.ReadByte();

         // This is the special case - this object references other objects, but those
         // references aren't specified like they are in all the other cases.  To make
         // things standard, we're just filling out a ReferencedObject with unused
         // x/y location data
         for (var i = 0; i < this.NumberOfItems; i++)
            this.Children.Add(new ReferencedObject(reader.ReadUInt16()) { XLocation = 0, YLocation = 0 });

         for (var i = 0; i < macroCount; i++)
         {
            this.Macros.Add(new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            });
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.InputList);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((byte)this.Value);
         writer.Write((byte)this.NumberOfItems);
         writer.Write((byte)this.Options);
         writer.Write((byte)this.Macros.Count);
         foreach (var o in this.Children)
            writer.Write((UInt16)o.ObjectId);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
