﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   [Flags]
   public enum ArchedBarGraphOptions
   {
      DrawBorder = 0x01,
      DrawTarget = 0x02,
      GraphType = 0x08,
      Deflection = 0x10,
   }

   // Annex B.11.4 Output Arched Bar Graph Object
   [ObjectType(IsobusObjectType.OutputArchedBarGraph)]
   public class OutputArchedBarGraph : IsoVtObject
   {
      public string Type { get { return "outputArchedBarGraph"; } }
      
      public Point Size { get; set; }
      public int Width { get { return Size.X; } }
      public int Height { get { return Size.Y; } }
      public int Colour { get; set; }
      public int TargetLineColour { get; set; }
      public ArchedBarGraphOptions Options { get; set; }
      public int StartAngle { get; set; }
      public int EndAngle { get; set; }
      public int BarGraphWidth { get; set; }
      public int MinValue { get; set; }
      public int MaxValue { get; set; }
      public int VariableReference { get; set; }
      public int Value { get; set; }
      public int TargetValueVariableReference { get; set; }
      public int TargetValue { get; set; }

      public IList<ReferencedMacro> Macros { get; set; }

      public OutputArchedBarGraph(int objectId, string name) : base(objectId, name)
      {
         Colour = 0;
         TargetLineColour = 12;
         this.Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         //this.Width = reader.ReadUInt16();
         //this.Height = reader.ReadUInt16();
         this.Colour = reader.ReadByte();
         this.TargetLineColour = reader.ReadByte();
         this.Options = (ArchedBarGraphOptions)reader.ReadByte();
         this.StartAngle = reader.ReadByte();
         this.EndAngle = reader.ReadByte();
         this.BarGraphWidth = reader.ReadUInt16();
         this.MinValue = reader.ReadUInt16();
         this.MaxValue = reader.ReadUInt16();
         this.VariableReference = reader.ReadUInt16();
         this.Value = reader.ReadUInt16();
         this.TargetValueVariableReference = reader.ReadUInt16();
         this.TargetValue = reader.ReadUInt16();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.OutputArchedBarGraph);
         writer.Write((UInt16)this.Size.X);
         writer.Write((UInt16)this.Size.Y);
         writer.Write((byte)this.Colour);
         writer.Write((byte)this.TargetLineColour);
         writer.Write((byte)this.Options);
         writer.Write((byte)this.StartAngle);
         writer.Write((byte)this.EndAngle);
         writer.Write((UInt16)this.BarGraphWidth);
         writer.Write((UInt16)this.MinValue);
         writer.Write((UInt16)this.MaxValue);
         writer.Write((UInt16)this.VariableReference);
         writer.Write((UInt16)this.Value);
         writer.Write((UInt16)this.TargetValueVariableReference);
         writer.Write((UInt16)this.TargetValue);
         writer.Write((byte)this.Macros.Count);
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }

}
