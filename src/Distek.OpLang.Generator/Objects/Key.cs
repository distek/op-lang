using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Distek.OpLang.Generator
{
   // Annex B.6 Key Object
   [ObjectType(IsobusObjectType.SoftKey)]
   public class Key : IsoVtObject, IHasChildren
   {
      public string Type { get { return "key"; } }

      public int BackgroundColour { get; set; }
      public int KeyCode { get; set; }

      public IList<ReferencedObject> Children { get; set; }
      public IList<ReferencedMacro> Macros { get; set; }

      public Key(int objectId, string name) : base(objectId, name)
      {
         BackgroundColour = 5;
         Children = new List<ReferencedObject>();
         Macros = new List<ReferencedMacro>();
      }

      public override IsoVtObject Read(BinaryReader reader, int objectId)
      {
         this.ObjectId = objectId;
         this.BackgroundColour = reader.ReadByte();
         this.KeyCode = reader.ReadByte();
         var objectCount = reader.ReadByte();
         var macroCount = reader.ReadByte();
         for (var i = 0; i < objectCount; i++)
         {
            var o = new ReferencedObject()
            {
               ObjectId = reader.ReadUInt16(),
               XLocation = reader.ReadInt16(),
               YLocation = reader.ReadInt16()
            };
            this.Children.Add(o);
         }
         for (var i = 0; i < macroCount; i++)
         {
            var o = new ReferencedMacro()
            {
               EventId = reader.ReadByte(),
               MacroId = reader.ReadByte(),
            };
            this.Macros.Add(o);
         }
         return this;
      }

      public override void Write(BinaryWriter writer)
      {
         writer.Write((UInt16)this.ObjectId);
         writer.Write((byte)IsobusObjectType.SoftKey);
         writer.Write((byte)this.BackgroundColour);
         writer.Write((byte)this.KeyCode);
         writer.Write((byte)this.Children.Count);
         writer.Write((byte)this.Macros.Count);
         foreach(var c in this.Children)
         {
            writer.Write((UInt16)c.ObjectId);
            writer.Write((Int16)c.XLocation);
            writer.Write((Int16)c.YLocation);
         }
         foreach(var m in this.Macros)
         {
            writer.Write((byte)m.EventId);
            writer.Write((byte)m.MacroId);
         }
      }
   }
}
