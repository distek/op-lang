﻿using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   public class IopGenerator
   {
      public IopGenerator()
      {
         _objectIds = new Dictionary<string, int>();
         _objectCounter = 0;
      }

      // Takes an object pool and generates an .iop file
      public IEnumerable<IsoVtObject> Generate(ParsedPool pool)
      {
         var objects = new List<IsoVtObject>();
         // add all constants to the dictionary so they get resolved
         foreach (var c in pool.Constants)
         {
            _objectIds.Add(c.Identifier, (int)c.Value.Get());
         }

         // create isobus objects from nodes
         foreach (var poolObject in pool.Objects)
         {
            Console.WriteLine("Creating {0}", poolObject.Name);
            // remember: can't populate properties here because we don't have all the object ids
            var actualType = ConcreteObject(poolObject);
            if (actualType != default(Type))
            {
               objects.Add(CreateIsoObject(actualType, poolObject));
            }
            else
            {
               var template = pool.Components.FirstOrDefault(c => c.Name.Equals(poolObject.Identifier));
               if (template != default(Component))
               {
                  foreach (var templateObject in template.Objects)
                  {
                     actualType = ConcreteObject(templateObject);
                     var name = poolObject.Name + "." + templateObject.Name;
                     var iso = Activator.CreateInstance(actualType, _objectCounter, name);
                     _objectIds.Add(name, _objectCounter);
                     _objectCounter++;
                     objects.Add((IsoVtObject)iso);
                  }
               }
            }
         }

         // populate isobus object values
         foreach (var iso in objects)
         {
            var node = pool.Objects.Where(o => o.Name.Equals(iso.Name));
            if (node.Any())
               PopulateProperties(node.First(), iso);
            else
            {
               // find the component child
               var c = pool.Objects.Where(x => iso.Name.StartsWith(x.Name)).First();
               if (c != null)
               {
                  var component = pool.Components.Where(x => x.Name.Equals(c.Identifier)).First();
                  var template = component.Objects.First(a => a.Name.Equals(iso.Name.Substring(iso.Name.IndexOf(".") + 1)));
                  PopulateProperties(template, iso);
                  PopulateProperties(c, iso);
               }
            }
         }

         return objects;
      }

      private Type ConcreteObject(IsoObject o)
      {
         if (_knownTypes.ContainsKey(o.Identifier))
            return _knownTypes[o.Identifier];
         return default(Type);
      }

      private IsoVtObject CreateIsoObject(Type type, IsoObject obj)
      {
         var iso = Activator.CreateInstance(type, _objectCounter, obj.Name);
         _objectIds.Add(obj.Name, _objectCounter);
         _objectCounter++;

         return (IsoVtObject)iso;
      }

      private void PopulateProperties(IsoObject node, IsoVtObject iso)
      {
         Console.WriteLine("Populating {0} from {1}", node.Name, iso.Name);
         foreach (var value in node.Value)
         {
            var prop = iso.GetType().GetProperty(value.Identifier);
            if (prop != null)
            {
               if (value.Value.ValueType == IsoType.Reference)
               {
                  Console.WriteLine("reference: {0}", value.Value.Value);
                  // lol
                  var id = _objectIds[value.Value.Value];
                  prop.SetValue(iso, id);
               }
               else
               {
                  if (prop.PropertyType == typeof(float))
                  {
                     var f = float.Parse(value.Value.Value);
                     prop.SetValue(iso, f);
                  }
                  else
                  {
                     prop.SetValue(iso, value.Value.Get());
                  }
               }
            }
         }
         if (node.Children != null)
         {
            foreach (var c in node.Children)
            {
               // want to find the first key that starts with c.Name
               var id = -1;
               if (_objectIds.ContainsKey(c.Name))
               {
                  id = _objectIds[c.Name];
                  ((IHasChildren)iso).Children.Add(new ReferencedObject(id) { XLocation = c.Location.X, YLocation = c.Location.Y });
               }
               else
               {
                  Console.WriteLine("looking for object {0} in iso {1}, node: {2}", c.Name, iso.Name, node.Name);
                  id = _objectIds.FirstOrDefault(x => x.Key.StartsWith(c.Name)).Value;
                  if (id > 0)
                  {
                     ((IHasChildren)iso).Children.Add(new ReferencedObject(id) { XLocation = c.Location.X, YLocation = c.Location.Y });
                  }
                  else
                  {
                     var first = iso.Name.Substring(0, iso.Name.IndexOf("."));
                     var second = c.Name;
                     var n = first + "." + second;
                     Console.WriteLine("resolve name to  {0}", n);
                     id = _objectIds[n];
                     //id = _objectIds.FirstOrDefault(x => x.Key.EndsWith(c.Name)).Value;
                     ((IHasChildren)iso).Children.Add(new ReferencedObject(id) { XLocation = c.Location.X, YLocation = c.Location.Y });
                  }
               }
            }
         }
      }

      /*
      i have everything except populating templated object children working so far,
      but this is getting unwieldly.  what i really want is for everything to be a component
      partly to make generation easier ( can treat all objects the same) and partly
      to make default objects work right.  so for output string we'd have a component
      object for output string with defaults that are overriden by user defined instances
      what i think that means is that we will not go from parsed objects right to is objects,
      and instead have a factory that takes the instance definition and the template.  that's 
      kind of what we're already doing, except we have 2 paths
       */

      private Dictionary<string, int> _objectIds;
      private int _objectCounter;

      private Dictionary<string, Type> _knownTypes = new Dictionary<string, Type>()
      {
         { "AlarmMask", typeof(AlarmMask) },
         { "Button", typeof(Button) },
         { "Container", typeof(Container) },
         { "DataMask", typeof(DataMask) },
         { "FillAttributes", typeof(FillAttributes) },
         { "FontAttributes", typeof(FontAttributes) },
         { "InputAttributes", typeof(InputAttributes) },
         { "InputBoolean", typeof(InputBoolean) },
         { "InputList", typeof(InputList) },
         { "InputNumber", typeof(InputNumber) },
         { "InputString", typeof(InputString) },
         { "Key", typeof(Key) },
         { "LineAttributes", typeof(LineAttributes) },
         { "NumberVariable", typeof(NumberVariable) },
         { "ObjectPointer", typeof(ObjectPointer) },
         { "ArchedBarGraph", typeof(OutputArchedBarGraph) },
         // we can alias components here if we want
         { "OutputArchedBarGraph", typeof(OutputArchedBarGraph) },
         { "OutputEllipse", typeof(OutputEllipse) },
         { "Ellipse", typeof(OutputEllipse) },
         { "OutputLine", typeof(OutputLine) },
         { "Line", typeof(OutputLine) },
         { "OutputLinearBarGraph", typeof(OutputLinearBarGraph) },
         { "LinearBarGraph", typeof(OutputLinearBarGraph) },
         { "OutputMeter", typeof(OutputMeter) },
         { "Meter", typeof(OutputMeter) },
         { "OutputNumber", typeof(OutputNumber) },
         { "OutputPolygon", typeof(OutputPolygon) },
         { "Polygon", typeof(OutputPolygon) },
         { "OutputRectangle", typeof(OutputRectangle) },
         { "Rectangle", typeof(OutputRectangle) },
         { "OutputString", typeof(OutputString) },
         { "PictureGraphic", typeof(PictureGraphic) },
         { "SoftKeyMask", typeof(SoftKeyMask) },
         { "StringVariable", typeof(StringVariable) },
         { "WorkingSet", typeof(WorkingSet) },
      };
   }
}
