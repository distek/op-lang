﻿/*using Distek.OpLang.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   public class ImageEncoder
   {
      public byte[] Encode(string filename, PictureGraphicFormat format, PictureGraphicOptions options)
      {
         var bytes = new List<byte>();
         var bmp = new Bitmap(filename);
         if(options != 0)
         {
            throw new Exception("Options not supported");
         }
         for (var y = 0; y < bmp.Height; y++)
         {
            for (var x = 0; x < bmp.Width; x++)
            {
               var color = bmp.GetPixel(x, y);
               var isoColour = color.R, color.G, color.B);
               bytes.Add((byte)isoColour);
            }
         }
         if (format == PictureGraphicFormat.EightBitColor)
         {
            return bytes.ToArray();
         }
         else if (format == PictureGraphicFormat.FourBitColor)
         {
            var fourBit = new List<byte>();
            for (var i = 0; i < bytes.Count(); i += 2)
            {
               var hi = (byte)(bytes[i] << 4);
               var lo = (byte)(bytes[i + 1]);
               fourBit.Add((byte)(hi | lo));
            }
         }
         //else if(format == PictureGraphicFormat.Monochrome)
         //{
            // i forgot how the other formats work at the end of a row
         //}
         else
         {
            throw new Exception(string.Format("Format {0} not supported", format.ToString()));
         }

         return bytes.ToArray();
      }
   }
}*/
