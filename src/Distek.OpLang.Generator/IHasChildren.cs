﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   public interface IHasChildren
   {
      IList<ReferencedObject> Children { get; set; }
   }
}
