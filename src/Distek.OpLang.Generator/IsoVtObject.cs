﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   public abstract class IsoVtObject 
   {
      public int ObjectId { get; protected set; }

      public string Name { get; set; }

      public IsoVtObject(int objectId)
      {
         ObjectId = objectId;
      }

      public IsoVtObject(int objectId, string name)
      {
         ObjectId = objectId;
         Name = name;
      }

      public abstract IsoVtObject Read(BinaryReader reader, int objectId);

      public abstract void Write(BinaryWriter writer);
   }

   public class ObjectTypeAttribute : Attribute
   {
      public IsobusObjectType ObjectType { get; set; }

      public ObjectTypeAttribute(IsobusObjectType objectType)
      {
         this.ObjectType = objectType;
      }
   }

   public enum IsobusObjectType
   {
      WorkingSet = 0,
      DataMask = 1,
      AlarmMask = 2,
      Container = 3,
      SoftKeyMask = 4,
      SoftKey = 5,
      Button = 6,
      InputBoolean = 7,
      InputString = 8,
      InputNumber = 9,
      InputList = 10,
      OutputString = 11,
      OutputNumber = 12,
      OutputList = 37,
      OutputLine = 13,
      OutputRectangle = 14,
      OutputEllipse = 15,
      OutputPolygon = 16,
      OutputMeter = 17,
      OutputLinearBarGraph = 18,
      OutputArchedBarGraph = 19,
      PictureGraphic = 20,
      NumberVariable = 21,
      StringVariable = 22,
      FontAttributes = 23,
      LineAttributes = 24,
      FillAttributes = 25,
      InputAttributes = 26,
      ExtendedInputAttributes = 38,
      ObjectPointer = 27,
      Macro = 28,
      ColourMap = 39,
      GraphicsContext = 36,
      AuxObjectPointer = 33,
      WindowMask = 34,
      KeyGroup = 35,
      ObjectLabelReferenceList = 40,
      ExternalObjectDefinition = 41,
      ExternalReferenceName = 42,
      ExternalObjectPointer = 43,
      Animation = 44,
   };
}
