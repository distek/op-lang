﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distek.OpLang.Generator
{
   public class ReferencedObject
   {
      public int ObjectId { get; set; }

      private int _xLocation;
      public int XLocation
      {
         get { return _xLocation; }
         set { _xLocation = value; }
      }

      private int _yLocation;
      public int YLocation
      {
         get { return _yLocation; }
         set { _yLocation = value; }
      }

      public ReferencedObject()
      {
      }

      public ReferencedObject(int objectId)
      {
         ObjectId = objectId;
      }
   }

   public class ReferencedMacro
   {
      public int EventId { get; set; }
      public int MacroId { get; set; }

      public ReferencedMacro()
      {
      }
   }
}
