﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprache;
using Xunit;

namespace Distek.OpLang.Core.UnitTest
{
   public class IdentifierParsingSyntaxTests : IEnumerable<object[]>
   {
      // Here's how to make a test that tests a bunch of different data with only one test method. 
      // It's not very elegant
      private readonly List<object[]> testData = new List<object[]>()
      {
         new object[] { "Wrap:False,", "Wrap", false },
         new object[] { " Wrap:False   ,", "Wrap", false },
         new object[] { " Wrap :False    ,", "Wrap", false },
         new object[] { " Wrap :        False,", "Wrap", false },
         new object[] { "Wrap:        False,", "Wrap", false },
         new object[] { "Wrap:True,", "Wrap", true },
         new object[] { " Wrap:True   ,", "Wrap", true },
         new object[] { " Wrap :True    ,", "Wrap", true },
         new object[] { " Wrap :        True,", "Wrap", true },
         new object[] { "Wrap:        True,", "Wrap", true },
         new object[] { "Width:90,", "Width",  90 },
         new object[] { "Width : 0,", "Width",  0 },
         new object[] { "    Width  :-90,", "Width",  -90 },
         new object[] { "Width:  690   ,", "Width",  690 },
         new object[] { "    Width:90,     ", "Width",  90 },
         new object[] { "Width\t:90,", "Width",  90 },
         new object[] { "Text:'asdf',", "Text", "asdf" },
         // spaces should be preserved
         new object[] { "Text:'  asdf',", "Text", "  asdf" },
         new object[] { "Text:'°',", "Text", "°" },
         new object[] { "Text:'as\ndf',", "Text", "as\ndf" },
         new object[] { "Text:'qwert',", "Text", "qwert" },
         new object[] { "Text:'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',", "Text", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" },
         new object[] { "Text:'',", "Text", "" },
      };
      public IEnumerator<object[]> GetEnumerator() => testData.GetEnumerator();
      IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

      [Theory]
      [ClassData(typeof(IdentifierParsingSyntaxTests))]
      public void WithValidPair_Parse_ReturnsValueAndIdentifier(string encoded, string identifier, object value)
      {
         var p = new PoolParser();
         var actual = PoolParser.Value.Parse(encoded);

         Assert.Equal(identifier, actual.Identifier);
         Assert.Equal(value, actual.Value.Get());
      }

      [Fact]
      public void WithValidReference_ParseIdentifier_ReturnsValue()
      {
         var s = @"ActiveMask: [defaultMask],";
         var parsed = PoolParser.Value.Parse(s);
         Assert.Equal("ActiveMask", parsed.Identifier);
         Assert.Equal(IsoType.Reference, parsed.Value.ValueType);
      }

      [Fact]
      public void ParseFloatingPoint()
      {
         var s = "0.01";
         var val = PoolParser.IntegerValue.Parse(s);
         Assert.Equal("0.01", val.Value);
      }

      [Fact]
      public void ParseIntegerValue()
      {
         var s = "91";
         var val = PoolParser.IntegerValue.Parse(s);
         Assert.Equal("91", val.Value);
      }
   }
}
