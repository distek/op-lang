﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Sprache;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Distek.OpLang.Core.UnitTest
{
   public class PoolParsingTests
   {
      [Fact]
      public void WithValidMinimumPool_ParsePool_ReturnsAst()
      {
         // note: WorkingSet doesn't actually have a version label parameter, or width/height
            //WorkingSet : [name] {
         var pool = @"

            const HorizontalLeft: 0,
            const HorizontalCenter: 1,
            const HorizontalRight: 4,
            const Monochrome: 0,
            const FourBitColor: 1,
            const EightBitColor: 2,

            WorkingSet : myWs {
               ActiveMaskId: defaultMask,
               BackgroundColour: RGB(0, 153, 0),
               Width: 160,
               Height: 80,
               Selectable: true,
               VersionLabel: '      1',
               Children [
                  Reference(os, 5, 27)
               ]
            }
            OutputString : os {
               Size: (160, 51),
               Location: (0, 27),
               Transparent: True,
               BackgroundColour: RGB(255, 255, 255),
               Value: 'My App',
               Wrap: False,
               FontAttributes: workingSetFont,
               Justification: HorizontalCenter,
            }
            FontAttributes : workingSetFont
            {
               FontColour: RGB(0, 0, 0),
               FontSize: 4,
               FontType: 0,
               FontStyle: 0,
            }

            DataMask: defaultMask {
               BackgroundColour: RGB(255, 255, 255),
               Children [
                  Reference(testString, 50, 50)
                  Reference(testString2, 50, 150)
                  Reference(meter, 150, 150)
               ]
            }

            OutputString: testString {
               Size: (160, 51),
               Value: 'asdf!',
               FontAttributes: dmFont,
               HorizontalAlignment: HorizontalCenter,
               BackgroundColour: RGB(0, 51, 51),
            }
            OutputString: testString2 {
               Size: (160, 51),
               Value: 'hello',
               FontAttributes: dmFont,
               HorizontalAlignment: HorizontalCenter,
               BackgroundColour: RGB(0, 51, 51),
            }
            FontAttributes: dmFont {
               FontColour: RGB(255, 255, 255),
               FontSize: 6,
               FontType: 0,
               FontStyle: 0,
            }
            OutputMeter: meter {
	            Size: (200, 200),
	            StartAngle: 0,
	            EndAngle: 180,
            }
            InputNumber: input1 {
               Size: (100, 20),
	            FontAttributes: dmFont,
               Scale: 0.01,
            }
            PictureGraphic: pg {
               Filename: 'test.png',
               ActualWidth: 40,
               ActualHeight: 30,
               Format: EightBitColor,
            }
            ";

         var g = new PoolParser();
         var y = g.GeneratePool(pool, @".");

         Assert.NotNull(y);
         Assert.Equal(10, y.Objects.Count);

         //File.WriteAllText("oplangtest.json", JsonConvert.SerializeObject(y, new JsonSerializerSettings
         //{
         //   ContractResolver = new CamelCasePropertyNamesContractResolver()
         //}));

         //using (var os = new BinaryWriter(File.OpenWrite("oplangtest.iop")))
         //{
         //   foreach(var x in y)
         //      x.Write(os);
         //}
      }
   }

}
