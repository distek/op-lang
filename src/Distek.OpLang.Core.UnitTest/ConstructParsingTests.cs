﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Sprache;
using System.Drawing;
using Distek.OpLang.Generator;
using System.Collections;

namespace Distek.OpLang.Core.UnitTest
{
   public class ConstructParsingTests : IEnumerable<object[]>
   {
      // Here's how to make a test that tests a bunch of different data with only one test method. 
      // It's not very elegant
      private readonly List<object[]> testData = new List<object[]>()
      {
         new object[] { "Selectable: True,", "Selectable", true },
         new object[] { "Value: 1,", "Value", 1 },
         new object[] { "String: 'hi',", "String", "hi" },
      };
      public IEnumerator<object[]> GetEnumerator() => testData.GetEnumerator();
      IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

      [Fact]
      public void WithValidConstant_Parse_ReturnsValue()
      {
         var s = "const HorizontalCenter: 1,";
         var c = PoolParser.Constant.Parse(s);
         Assert.NotNull(c);
         Assert.Equal("HorizontalCenter", c.Identifier);
         Assert.Equal(1, c.Value.Get());
      }

      [Theory]
      [ClassData(typeof(ConstructParsingTests))]

      public void WithIdentifierValue_Parse_ReturnsValue(string code, string identifier, object expected)
      {
         var val = PoolParser.Value.Parse(code);
         Assert.Equal(expected, val.Value.Get());
      }

      [Fact]
      public void WithIdentifierValue_Parse_IsCorrectType()
      {
         var val1 = "Selectable: True,";
         var val2 = "Value: 1,";
         var val3 = "String: 'hi',";

         var booleanVal = PoolParser.Value.Parse(val1);
         var numberVal = PoolParser.Value.Parse(val2);
         var stringVal = PoolParser.Value.Parse(val3);

         Assert.Equal(IsoType.Boolean, booleanVal.Value.ValueType);
         Assert.Equal(IsoType.Number, numberVal.Value.ValueType);
         Assert.Equal(IsoType.String, stringVal.Value.ValueType);
      }

      [Fact]
      public void WithIdentifierObject_Parse_ReturnsValue()
      {
         var isoObject = @"OutputString : outNum {
         Integer: 1,
         Transparent: True,
         Value: 'My App',
         Wrap: false, }";

         var collection = PoolParser.PoolObject.Parse(isoObject);

         Assert.Equal("OutputString", collection.Identifier);
         Assert.Equal("Integer", collection.Value.ElementAt(0).Identifier);
         Assert.Equal(1, collection.Value.ElementAt(0).Value.Get());
         Assert.Equal("outNum", collection.Name);
      }

      [Fact]
      public void WithIdentifierObject_Parse_ReturnsObject()
      {
         var isoObject = @"OutputNumber {
         Width: 5,
         Integer: 1,
         Transparent: True,
         Value: 5,
         Wrap: false, }";

         //var collection = parser.GenerateObject(isoObject);

         //Assert.Equal(5, ((OutputNumber)collection).Value);
         //Assert.Equal("Integer", collection.Value.ElementAt(0).Identifier);
         //Assert.Equal(1, collection.Value.ElementAt(0).Value.Get());
      }

      [Fact]
      public void WithIdentifierIntegerPair_Parse_ReturnsValue()
      {
         var val = " 200";

         var intVal = PoolParser.IntegerValue.Parse(val);

         Assert.Equal(200, intVal.Get());
      }

   
      [Fact]
      public void Point_Parse_ReturnsValue()
      {
         var val = "( 1 , 2 )";

         var pointVal = PoolParser.PointValue.Parse(val);
         var testPoint = new Point(1, 2);

         Assert.Equal(testPoint, pointVal.PointValue);
      }

      [Fact]
      public void Color_Parse_ReturnsValue()
      {
         var val = " RGB( 255 , 255, 255 ), ";

         var colorVal = PoolParser.Colour.Parse(val);
         //var testColor = Color.FromArgb(1, 2, 3);

         //Assert.Equal(testColor, colorVal.ColorValue);
         Assert.Equal(1, colorVal.ColorValue);
      }


      [Fact]
      public void WithIdentifierRgbColorPair_Parse_ReturnsValue()
      {
         var val = "BackgroundColour: RGB(127, 0, 255),";

         // Probably want to use IsoColour here once we have VT code
         var actual = PoolParser.Value.Parse(val);

         Assert.Equal("BackgroundColour", actual.Identifier);
     }

      [Fact]
      public void WithIdentifierIndexedColorPair_Parse_ReturnsValue()
      {
         var val = "BackgroundColour: Colour(118),";

         // Probably want to use IsoColour here once we have VT code
         var actual = PoolParser.Value.Parse(val);

         Assert.Equal("BackgroundColour", actual.Identifier);
         // These values are the rgb of the color at index 118
         Assert.Equal(118, actual.Value.Get());
      }

      //[Fact]
      public void WithValidFileName_Import_ParsesName()
      {
         // don't have a good way to set the path right now
         var val = "import test.mask";

         var actual = PoolParser.Import.Parse(val);
         var expected = new FileReference("test.mask", @"c:\temp");

         Assert.Equal(expected, actual);
      }
   }
}
