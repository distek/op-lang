﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprache;
using Xunit;

namespace Distek.OpLang.Core.UnitTest
{
   public class ObjectParsingTests
   {
      [Fact]
      public void WithChildlessButton_ParsePool_ReturnsButton()
      {
         // Arrange
         var s = "Button: name { Width: 160, Height: 80, }";

         // Act
         var parsedButton = PoolParser.PoolObject.Parse(s);

         Assert.NotNull(parsedButton);
         Assert.Equal(160, parsedButton.Value.ElementAt(0).Value.Get());
         Assert.Equal(80, parsedButton.Value.ElementAt(1).Value.Get());
      }

      [Fact]
      public void WithValidOutputString_ParseObject_ReturnsObject()
      {
         var s = @"OutputString : name {
                     Text: 'hello',
                     Width: 80,
                  }";

         var parsed = PoolParser.PoolObject.Parse(s);

         Assert.Equal("OutputString", parsed.Identifier);
         Assert.Equal("hello", parsed.Value.ElementAt(0).Value.Get());
         Assert.Equal(80, parsed.Value.ElementAt(1).Value.Get());
      }

      [Fact]
      public void WithValidOutputNumber_ParseObject_ReturnsObject()
      {
         var s = @"OutputNumber :name {
                     Text: 'myval',                     Width: 80,
                  }";

         var parsed = PoolParser.PoolObject.Parse(s);

         Assert.Equal("OutputNumber", parsed.Identifier);
         Assert.Equal("myval", parsed.Value.ElementAt(0).Value.Get());
         Assert.Equal(80, parsed.Value.ElementAt(1).Value.Get());
      }

      [Fact]
      public void WithChildArray_Parse_ReturnsObject()
      {
         var s = @"Children [ Reference(one, 0, 0) reference(two, 2, 2) ]";
         var parsed = PoolParser.ChildArray.Parse(s);
         Assert.NotNull(parsed);
      }

      [Fact]
      public void WithWorkingSetWithChildren_ParseObject_ReturnsObject()
      {
         var s = @"WorkingSet : ws {
            Children [
                  Reference(test, 9, 5)
               ]
         }";

         var parsed = PoolParser.PoolObject.Parse(s);
         Assert.Equal("WorkingSet", parsed.Identifier);
      }

      [Fact]
      public void WithValidObjectPointer_Parse_ReturnsObject()
      {
         var code = @"
            ObjectPointer: testPointer {
               Value: pointedObject,
            }
            ";
         var parsed = PoolParser.PoolObject.Parse(code);
         Assert.NotNull(parsed);
         Assert.Equal("pointedObject", parsed.Value.First().Value.Value);
      }

      [Fact]
      public void WithValidButton_Parse_ReturnsObject()
      {
         var code = @"
            Button: testButton {
               Size: (100, 30),
            }
            ";
         var parsed = PoolParser.PoolObject.Parse(code);
         Assert.NotNull(parsed);
      }

      [Fact]
      public void ObjectPointerWeirdness()
      {
         var code = @"
            ObjectPointer: keyPointer {
               Value: a,
            }
            Key: a {
               BackgroundColour: colour(8),
               Children [
                  Reference(keyText, 0, 20)
               ]
            }
         ";
         var parsed = PoolParser.Pool.Parse(code);
         Assert.Equal(2, parsed.Objects.Count());
      }

      [Theory]
      [InlineData("a")]
      [InlineData("b")]
      [InlineData("c")]
      [InlineData("d")]
      [InlineData("e")]
      [InlineData("f")]
      [InlineData("g")]
      [InlineData("h")]
      [InlineData("i")]
      [InlineData("j")]
      [InlineData("k")]
      [InlineData("l")]
      [InlineData("m")]
      [InlineData("n")]
      [InlineData("o")]
      [InlineData("p")]
      [InlineData("q")]
      [InlineData("r")]
      [InlineData("s")]
      [InlineData("t")]
      [InlineData("u")]
      [InlineData("v")]
      [InlineData("w")]
      [InlineData("x")]
      [InlineData("y")]
      [InlineData("z")]
      [InlineData("ttttt")]
      public void WithReferenceValueStartCharacter_ParseObject_ReturnsValue(string name)
      {
         // error: 'c', 'f', 'r', 't'
         // keywords? false, true, colour, rgb
         var code = string.Format("ObjectPointer: myPointer {{ Value: {0}, }}", name);
         var parsed = PoolParser.PoolObject.Parse(code);
         Assert.NotNull(parsed);
      }
   }
}
