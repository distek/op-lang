﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Sprache;

namespace Distek.OpLang.Core.UnitTest
{
   public class ComponentTests
   {
      [Fact]
      public void WithValidComponent_Parse_ReturnsAst()
      {
         // the component body contains properties the using code can set
         var code = @"
            Component : TextButton  {
               Location: b.Location,
               Size: b.Size,
               Value: buttonText.Value,

               [
                  Button: b {
                     Children [
                        Reference(buttonText, 0, 0)
                     ]
                  }
                  OutputString: buttonText {
                     Text: 'button!',
                     Size: (60, 20),
                  }
               ]
            }
         ";

         var c = ComponentGrammar.Component.Parse(code);
         Assert.NotNull(c);
      }

      [Fact]
      public void WithReferencedComponent_PoolParse_ReturnsAst()
      {
         // the component body contains properties the using code can set
         var code = @"
            Component : TextButton  {
               Location: b.Location,
               Size: b.Size,
               Value: buttonText.Value,

               [
                  Button: b {
                     Children [
                        Reference(buttonText, 0, 0)
                     ]
                  }
                  OutputString: buttonText {
                     Text: 'button!',
                     Size: (60, 20),
                  }
               ]
            }
            TextButton: myButton {
               Value: 'asdf',
            }
         ";

         var c = PoolParser.Pool.Parse(code);
         Assert.NotNull(c);
      }

      [Fact]
      public void WithReferencedComponent_PoolGenerate_CreatesPool()
      {
         // the component body contains properties the using code can set
         var code = @"
            Component : TextButton  {
               Location: b.Location,
               Size: b.Size,
               Value: buttonText.Value,

               [
                  Button: myButton {
                     Children [
                        Reference(buttonText, 0, 0)
                     ]
                  }
                  OutputString: buttonText {
                     Text: 'button!',
                     Size: (60, 20),
                  }
               ]
            }

            DataMask: cmask {
               Children [ Reference(myButton, 0, 0) ]
            }

            TextButton: myButton {
               Value: 'asdf',
            }
         ";
         var generator = new PoolParser();
         var generated = generator.GeneratePool(code, ".");
         Assert.NotNull(generated);
         Assert.Equal(2, generated.Objects.Count());
      }

      [Fact]
      public void WithMultipleReferencedComponents_PoolGenerate_CreatesPool()
      {
         // the component body contains properties the using code can set
         var code = @"
            Component : TextButton  {
               Location: b.Location,
               Size: b.Size,
               Value: buttonText.Value,

               [
                  Button: myButton {
                     Children [
                        Reference(buttonText, 0, 0)
                     ]
                  }
                  OutputString: buttonText {
                     Text: 'button!',
                     Size: (60, 20),
                  }
               ]
            }

            DataMask: cmask {
               Children [ Reference(myButton, 0, 0) ]
            }

            TextButton: myButton {
               Value: 'asdf',
            }
            TextButton: myOtherButton {
               Value: 'asdf',
            }
         ";
         // i need a way to generalize the name for the component instance

         var generator = new PoolParser();
         var generated = generator.GeneratePool(code, ".");
         Assert.NotNull(generated);
         Assert.Equal(3, generated.Objects.Count());
      }
   }
}
